.. _adjectives:

========================================
Прилагательные / Pridevi
========================================

.. toctree::
   :maxdepth: 1

   adjectives/possessive-adjectives



.. csv-table::
   :widths: 30, 30
   :header: "Сербский", "Русский"
   :delim: ;

   ``velik``; большой, великий