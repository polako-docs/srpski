.. _possessive-adjectives:

=======================================
Притяжательные прилагательные
=======================================


**Притяжательные прилагательные** — это слова, которые выражают признак принадлежности предмету (кроличий хвост, Васин рюкзак). В русском языке вместо притяжательного прилагательного чаще используется конструкция с родительным падежом (рюкзак Наташи), а в сербском – наоборот.

Притяжательные прилагательные образуются следующим образом:

.. csv-table::
   :widths: 30, 10, 30
   :header: "Условие", "Суффикс", "Пример"
   :delim: ;

   Если основа существительного оканчивается на согласные ``č``, ``ć``, ``dž``, ``đ``, ``ž``, ``š``, ``j``, ``lj``, ``nj`` и имеет окончание ``о``, ``е``, ``∅``;  ``-ев``/``-ev``; Ивић -- Ивићев, Миливоје -- Миливојев, Карађорђе -- Карађорђеva šnicla
   Если основа существительного оканчивается на остальные согласные и имеет окончание ``о``, ``е``, ``∅``; ``-ов``/``-ov``; Марко -- Марков, Павле — Павлов, Милан – Миланов
   Если существительное заканчивается на ``-a``; ``ин``/``-in``; Nemanja -- Nemanjina ulica, Višnja -- Višnjina ulica


.. warning:: Но это все примерно, существуют исключения и другие правила. Например, существует имя Раде, но никто не скажет Радов.

При образовании притяжательных прилагательных от имен, оканчивающихся на ``-ица``, происходит чередование ``ц/ч``:
Зорица — Зоричин, Драгица — Драгичин, Милица — Миличин.

.. note:: Притяжательные прилагательные, образованные от личных имен и фамилий, пишутся в сербском языке с большой буквы


Склонение притяжательных прилагательных
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Miloš je Stefanov i Jelenin sin.
* Marija je Steafanova i Jelenina ćerka.
* Miloš je Marijin brat.


Прилагательные на -ski, -čki, -ški
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Если **основа слова**, от которого образовано прилагательное, оканчивается на ``-Č``, ``-K`` или ``-C``, то окончание будет ``-ČKI``:

* Vršac -- vršački
* Subotica -- subotički
* Nemačka -- nemački


Если **основа** слова, от которого образовано прилагательное, оканчивается на ``-Š``, ``-G`` или ``-H`` то окончание будет ``-ŠKI``:

* Niš -- niški
* Prag -- praški
  
В остальных случаях окончание будет ``-SKI``:

*  Beograd -- Beogradski
*  Sava -- Savski
*  Dunav -- Dunavski
  

.. csv-table::
   :widths: 30, 10, 30
   :header: "Окончание основы", "Окончание слова", "Пример"
   :delim: ;

   ``-Č``, ``-K``, ``-C``; ``-ČKI``; Vršac -- vršački, Subotica -- subotički, Nemačka -- nemački
   ``-Š``, ``-G``, ``-H``; ``-ŠKI``; Niš -- niški, Prag -- praški
   В остальных случаях; ``-SKI``; Beograd -- Beogradski, Sava -- Savski, Dunav -- Dunavski



Примеры
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Nemački automobili su odlični.
* Američke komedije su popilarne.
* Ovde je Zvezdarska šuma.

