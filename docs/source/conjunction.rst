.. _conjunction:

=======================================
Союзы / Veznici
=======================================




Союз DA в придаточных предложениях (misliti, znati)
---------------------------------------

Союз DA используется еще и в придаточных предложениях, после глаголов типа misliti, znati (там, где в русском используется „думаю, что...“, „знаю, что...“).

* Mislim da on spava.
* Znam da vi ne radite.
	  
.. attentiom:: Неправильно будет: Mislim šta on spava. Znam šta vi ne radite.
