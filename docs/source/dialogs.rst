.. _dialogs:


======================================
Диалоги
======================================

.. note:: Последнее обновление |today|

.. toctree::
   :maxdepth: 1

   dialogs/greetings
   dialogs/in-transport
   .. dialogs/in-shop
