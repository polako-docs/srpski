.. _cases:


Падежи / Padeži
=======================================

**Падеж** — форма имени существительного, которая выражает его взаимосвязь с другими словами в предложении или словосочетании. Все падежи отвечают на определённые вопросы.

.. toctree::
   :maxdepth: 1

   cases/accusative
   cases/lokativ
   cases/dativ
   cases/genitiv
   cases/vokativ
   cases/instrumental
   cases/cases-cheatsheet
   

.. csv-table::
   :widths: 30, 30, 40
   :delim: ;
   :header: "Падеж", "Padež", "Odgovaraju  na pitanja"

   Именительный (основная форма существительного); ``Nominativ`` (osnovni oblik imenice); ``Ko?``, ``Šta?`` (Есть Кто? Что?)
   Родительный (говорит о принадлежности); ``Genitiv`` (kazuje pripadnost); ``(Od) Koga?``, ``Čega``, ``Čiji`` (Нет Кого? Чего? Чей?)
   Дательный (показывает направление); ``Dativ`` (kazuje pravac i namenu); ``Kome?``, ``Čemu?`` (Рад Кому? Чему?)
   Винительный (показывает объект, на который направлено действие); ``Akuzativ`` (kazuje objekat); ``Koga?``, ``Šta (vidiš)?``, ``Kuda (ideš)?`` (Вижу Кого? Что?)
   Звательный (в русском как бы есть и как бы нет); ``Vokativ`` (služi za dozivanje i obraćanje); ``Hej!`` (Эй!)
   Творительный; ``Instrumental`` (kazuje društvo i sredstvo); ``(Sa) kim?`` ``Čim(e)?`` (Доволен Кем? Чем?)
   Предложный (называет объект мысли или речи); ``Lokativ`` (označava mesto i nepravi objekat); ``Gde``, ``O kome``, ``O čemu`` (Думаю О ком? О чём? В ком? В чём? Где? (Местный))

