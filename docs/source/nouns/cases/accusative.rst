.. _accusative:


Винительный падеж / Akuzativ (koga?, šta?)
===========================================


**Винительный падеж** (по-сербски Акузатив) -- выражает на кого или на что направлено действие. Отвечает на вопрос KOGA? ŠTA? (Кого?, Что?). 

Винительный падеж используется в функции прямого дополнения (Volim svoju sestru) и в значении направления (Idem u Moskvu).


**Окончания винительного падежа в единственном числе:**

.. csv-table::
   :widths: 20, 40, 40
   :delim: ;
   :header: "Род", "Именительный падеж", "Винительный падеж"

   Мужской; Ovo je moj novi telefon.; Volim moj novi telefon.
   Мужской; Ovo je moj novi professor.; Volim mog novog professora.
   Женский; Ovo je moja nova torba.; Volim moju novu torbu.
   Женский; Ovo je moja nova stvar.; Volim moju novu stvar.
   Средний; Ovo je moje lepo selo.; Volim moje lepo selo.


В сербском языке одушевлённые и неодушевлённые существительные мужского рода в винительном падеже имеют разные окончания. Окончание прилагательных мужского рода может быть и ``-eg``, после мягких и шипящих (c, č, ć, dž, đ, š, ž, lj, nj, j): naš – našeg, tuđ – tuđeg. 


**Окончания винительного падежа во множественном числе:**

.. csv-table::
   :widths: 20, 40, 40
   :delim: ;
   :header: "Род", "Именительный падеж", "Винительный падеж"

   Мужской; Ovo su moji novi telefoni.; Volim moje nove telefone.
   Мужской; Ovo su moji novi professori.; Volim moje nove professore.
   Женский; Ovo su moje nove torbe.; Volim moje nove torbe.
   Женский; Ovo su moje nove stvari.; Volim moje nove stvari.
   Средний; Ovo su moja lepa sela.; Volim moja lepa sela.
   Средний; Ovo su moja mala deca.; Volim moju malu decu.


.. note:: Исключение слово ``deca`` (дети) в винительном падеже ведет себя как женский род в единственном чисел ``decu``.

.. attention:: Слово ``stvar`` женского рода, во множественном числе в винительном падеже имеет окончание ``-i``, а не ``-e`` (``stvari``) 


Исключения во множественном числе мужского рода
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Vidim lepe grad ``ove`` -- в существительных мужского рода на ``-ov``/ ``-ev`` основа слова во множественном числе удлинялась (grad -- gradovi). Окончания винительного падежа добавляются на эту основу.

2. Volim moje učeni ``ke`` i moje psihologe -- в существительных мужского рода, где во множественном числе в окончаниях происходит чередование букв ``k`` и ``g`` перед окончанием ``i`` (učenik – učenici; psiholog – psiholozi) в винительном падеже чередования нет. Так как в конце добавляется ``-e``, а не ``-i``.
   

Функция прямого дополнения
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Функция прямого дополнения люблю + что, вижу + что. Используется с глаголами:


.. csv-table::
   :widths: 20, 20, 20, 20, 20
   :delim: ;

   čitati; gledati; tražiti; znati; videti
   slušati; kuvati; učiti; voziti; svirati
   imati; spremati; želiti; trenirati; nositi
   nemati; uzimati; voleti; raditi; koristiti


Глаголы igrati, ručati, večerati в винительном падеже отличаются от русского языка тем, что в русском языке говорят ``играем во что-то``, в сербском говорят ``igram``
