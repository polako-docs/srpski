.. _cases-cheatsheet:


==================================================
Шпаргалка по падежам
==================================================

В сербском языке, как и в русском, существительные, прилагательные и местоимения 
изменяются (склоняются [deklinacija]) по родам, числам и падежам.


Именительный падеж / Nominativ
----------------------------------------------------

Вопросы: ``Ko? Što?`` (Есть Кто? Что?)


.. csv-table::
   :widths: 10, 20, 20
   :delim: ;
   :header: "Rod" , "Jednina (прилагательное-существительное)", "Množina"

   m.r.; ``-Ø/-i`` ``-Ø/-o``; ``-i`` ``-i``
   ž.r.; ``-a`` ``-a/-Ø``; ``-e`` ``-e``
   s.r.; ``-o`` ``-o/-e``; ``-a`` ``-a``
   
Исключения:

* существительные мужского рода в единственном числе могут заканчиваться на ``-a``, во множественно числе они имеют окончание ``-e``, также как и согласуемые с ними прилагательные и местоимения.
* существительные женского рода в единственном числе могут заканчиваться на соглсную, во множественном числе они имеют окончание ``-i``, также как и согласуемые с ними прилагательные и местоимения.


Родительный падеж / Genitiv
----------------------------------------------------


Дательный падеж / Dativ
----------------------------------------------------

Отвечате на вопросы: ``kome? čemu?`` (кому? чему?). Дательный падеж показывает направление и цель. 

.. csv-table::
   :widths: 10, 20, 20
   :delim: ;
   :header: "Rod" , "Jednina (прилагательное-существительное)", "Množina"

   m.r./s.r.; ``-om/-em`` ``u``; ``-im`` ``-ima``
   ž.r.; ``-oj`` ``-i``; ``-im`` ``-ama``


Окончания дательного падежа полностью совпадают с окончаниями
предложного падежа, и в единственном, и во множественном числе.

Винительный падеж / Akuzativ
----------------------------------------------------


Звательный падеж / Vokativ
----------------------------------------------------



Предложный падеж / Lokativ 
----------------------------------------------------

Вопросы: ``gde? o kome? o čemu?`` (где? о ком? о чем?)




Творительный падеж / Instrumental
----------------------------------------------------

Вопросы: ``kim(e)? cime?`` (о ком? о чем?)

Обозначает: Объект, инструмент, с помощью которого совершается действие, или различные 
обстоятельства действия (время, место и пр.).

Окончания в товрительном падеже:

.. csv-table::
   :widths: 10, 20, 20
   :delim: ;
   :header: "Rod" , "Jednina (прилагательное-существительное)", "Množina"

   m.r./s.r.; ``-im`` ``-om/-em``; ``-im`` ``-ima``
   ž.r.; ``-om`` ``-om/-ju/-i``; ``-im`` ``-ama``


Исключения:

* в среднем роде ``dete`` -- ``sa detetom``
* в женском роде ``radost`` -- ``sa radošću``