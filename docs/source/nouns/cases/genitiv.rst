.. _genitiv:

==============================================================
Родительный падеж / Genitiv (koga? čega? odakle?)
==============================================================

Родительный падже отвечает на вопросы: ``кого/koga?`` ``чего/čega?`` ``откуда/odakle?``

Окончания родительного падежа и в единственном и во множественном числе:

.. csv-table::
   :widths: 10, 20, 20
   :delim: ;
   :header: "Lice" , "Jednina", "Množina"

   m.l. ;``Ja sam iz malOG gradA.``; ``Mi smo iz malIH gradovA``
   m.l. ;``Došao sam kod našeg profesorA``; ``Došli smo kod naših profesora``
   ž.l. ;``Izašla sam iz starE kafanE``;``Ovde ima mnogo starIH kafanA``
   s.l. ;``Došao sam do novOG požorištA``;``Mi smo iz malIH selA``


Исключения во множественном числе:

* ``devojke`` -- ``devojaka``
* ``studenti`` -- ``studenata``
* ``pisma`` -- ``pisama``
* ``sat`` -- ``sati``
* ``mesec`` -- ``meseci``
* ``torba`` -- ``torbi``


Употребление родительного падежа
-----------------------------------------------------

С предлогами места / Predlozi za mesto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :widths: 10, 20, 20
   :delim: ;
   :header: "Predlog", "Prevod", "Primer"

   ``do``; ``до``; Uradite to do kraja nedelije
   ``od``; ``из, от, с``; To je salata od paradajza.
   ``iz``; ``из, с``; Ja sam iz Moskve.
   ``sa``; ``с, из``; Uzmi ova sa stola.
   ``ispred``; ``перед, впереди, спереди``; 
   ``iza``; ``за``; Iza ćoška nalazi se moja omiljena kafana.
   ``iznad``; ``над``; 
   ``ispod``; ``под``; 
   ``pored``; ``возле, рядом, помимо``; 
   ``blizu``; ``близко, рядом, вблизи, поблизости``; 
   ``kod``; ``у, к``;  Za rođendan idemo kod naših roditelja.
   ``oko``; ``вокруг``;
   ``između``; ``между``; 
   ``preko puta``; ``напротив``; Zgrada se nalazi prekoputa Narodnog muzeja. 


С предлогами веремени / Predlozi za vreme
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table::
   :widths: 10, 20, 20
   :delim: ;
   :header: "Predlog", "Prevod", "Primer"

   ``do``; ``до``;
   ``od``; ``от, с``;
   ``pre``; ``перед, тому назад``;
   ``posle``; ``затем, после того, потом``;
   ``tokom``; ``в течение, во время, на протяжении чего``;
   ``za vreme``; ``во время``;
   ``oko``; ``около``;

С другими предлогами
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. csv-table::
   :widths: 10, 20, 20
   :delim: ;
   :header: "Predlog", "Prevod", "Primer"

   ``bez``; ``без``; 
   ``zbog``; ``из-за``; Zbog lošeg vremena ne idemo na more.



С числительными / Sa brojevima
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* 2, 3, 4 + Gen. jedn.
* 5 ... 20 + Gen. množ.


Со словами
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. csv-table:: слово + род. ед. числа
   :widths: 20, 20
   :delim: ;
   :header: "Reč", "Prevod", "Primer"

   ``flaša``; бутылка;
   ``čaša``; стакан, рюмка;
   ``šolja``; чашка;
   ``kašika``; ложка;
   ``tanjir``; тарелка;
   ``porcija``; порция;
   ``litar``; литр;


.. csv-table:: слово + род. мн. числа
   :widths: 20, 20
   :delim: ;
   :header: "Reč", "Prevod", "Primer"

   ``mnogo``; много;
   ``puno``; много, уйма, навалом;
   ``malo``; мало;
   ``nekoliko``; несколько;
   ``koliko``; сколько;
   ``toliko``; столько;

В конструкциях
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* prošle godine
* prošlog meseca
* ove nedelje
* sledeće godine
* petog decembera


Значение принадлежности
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Bulevar kralja Aleksandra 
* knjiga moje mame
