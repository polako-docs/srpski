.. _lokativ:

Предложный падеж / Lokativ (gde? o kome? o čemu?)
==================================================

Локатив отвечает на вопросы ``o kome?``, ``o čemu?`` и используется только с предлогами (обычно ``u``, ``na``, ``o``, ``po``). 

Локатива указывает на то, что падеж употребляется в значении места, когда что-то происходит на каком-то месете, без движения.



**Окончания предложного падежа в единственном числе:**

.. csv-table::
   :widths: 20, 40, 40
   :delim: ;
   :header: "Род", "Именительный падеж", "Винительный падеж"

   Мужской; ``Ovo je moj novi professor.``; ``Pričam o mom novom professoru.``
   Мужской; ``Ovo jenaš grad.``; ``On živiu našem gradu.``
   Средний; ``Ovo je novo selo.``; ``Żivim u novom selu.``
   Женский род; ``Ovo je velika zemlja.``; ``Živimo u velikoj zemlji.``
   Женский род; ``Ovo je moja nova stvar.``; ``Pričam o mojoj novoj stvari.``


Исключения в среднем роде:

* dete -- o detetu
  
Исключения в женском роде:

* Страны с окончанием на ``-ska``, ``-čka``, ``-ška`` имеют окончание ``-oj``: Polska -- u Poljskoj
* Буквы ``k → c``: Amerika -- u Americi
* Буквы ``g → z``: knjiga -- u knjizi
* Буквы ``h → s``: epoha -- o eposi


**Окончания предложного падежа во множественном числе:**

.. csv-table::
   :widths: 20, 40, 40
   :delim: ;
   :header: "Род", "Именительный падеж", "Винительный падеж"

   Мужской; ``Ovo su moji novi professori.``; ``Pričam o mojim novim professorima.``
   Мужской; ``Ovo su naši gradovi.``; ``On radi u našim gradovima.``
   Средний; ``Ovo su nova sela.``; ``Pričam o novim selima.``
   Женский; ``Ovo su bogate zemllje.``; ``Radimo u bogatim zemljama.``
   Женский; ``Ovo su moje nove stvare.``; ``Pričam o mojim novim stvarima.``




Личные местоимения в предложном падеже
----------------------------------------------

.. csv-table::
   :widths: 10, 10,  20
   :delim: ;
   :header: "Местоимение", "Полная форма", "Перевод"

   ja; ``meni``; обо мне
   ti; ``tebi``; о тебе
   on/ono; ``njemu``; о нем
   ona; ``njoj``; о ней
   mi; ``nama``; о нас
   vi; ``vama``; о вас
   oni/one/ona; ``njima``; о них

