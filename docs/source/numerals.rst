
.. _numerals:

=======================================
Числительные / Brojevi
=======================================


Имя числительное — самостоятельная часть речи, которая обозначает число, количество и порядок предметов.

* количественные -- отвечают на вопрос «Сколько?» («один», «четырнадцать», «сто пятьдесят»);
* порядковые -- отвечают на вопрос «Который по счету?» («первый», «пятнадцатый», «стошестидесятый»);
* собирательные -— отвечают на вопрос сколько? (оба, двое, пятеро)

Подробнее
---------------------------------------

.. toctree::
   :maxdepth: 1

   numerals/cardinal-numerals
   numerals/mathematical-symbols
  
