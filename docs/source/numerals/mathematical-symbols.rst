.. _mathematical-symbols:


===============================================
Математические символы / Matematički simboli
===============================================


.. csv-table:: 
   :widths: 10, 20
   :delim: ;

   ``=``;``jednako``
   ``≠``;``nije jednako``
   ``≈``;``Približno jednako``
   ``*``;``zvezdica``
   ``#``;``taraba (hešteg)``
   ``.``;``tačka``
   ``,``;``zapeta (zarez)``
   ``-``;``crtica``
   ``—``;``crta``
   ``_``;``donja crta``
   ``/``;``kosa crta``
   ``@``;``et (majmunsko A)``
   ````;````
   ````;````


.. https://www.mathreference.org/index/page/id/424/lg/sr