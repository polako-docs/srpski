.. _colors:


=======================================
Цвета / Boje
=======================================

``Boja`` -- цвет, краска


.. |black| image:: ../_static/colors/black.png
	:scale: 30%

.. |white| image:: ../_static/colors/white.png
	:scale: 30%

.. |red| image:: ../_static/colors/red.png
	:scale: 30%

.. |lightgreen| image:: ../_static/colors/green-light.png
	:scale: 30%

.. |darkgreen| image:: ../_static/colors/green-dark.png
	:scale: 30%

.. |blue| image:: ../_static/colors/blue.png
	:scale: 30%

.. |lightblue| image:: ../_static/colors/blue-light.png
	:scale: 30%

.. |darkblue| image:: ../_static/colors/blue-dark.png
	:scale: 30%

.. |yellow| image:: ../_static/colors/yellow.png
	:scale: 30%

.. |brawn| image:: ../_static/colors/brawn.png
	:scale: 30%


.. |brawn-dark| image:: ../_static/colors/brawn-dark.png
	:scale: 30%

.. |brawn-light| image:: ../_static/colors/brawn-light.png
	:scale: 30%


.. |rose| image:: ../_static/colors/rose.png
	:scale: 30%


.. |violet| image:: ../_static/colors/violet.png
	:scale: 30%

.. |orange| image:: ../_static/colors/orange.png
	:scale: 30%

.. |grey| image:: ../_static/colors/grey.png
	:scale: 30%

.. |beige| image:: ../_static/colors/beige.png
	:scale: 30%

.. |turquoise| image:: ../_static/colors/turquoise.png
	:scale: 30%

.. |army-green| image:: ../_static/colors/army-green.png
	:scale: 30%

.. |space| unicode:: U+00A0


.. csv-table::
   :widths: 20, 20, 20, 30
   :header: "Сербский", "Цвет", "Русский", "Пример"
   :delim: ;

   ``crnа``; |black|; черный; cirni čaj, crna mačka, Crno more
   ``bela``; |white|; белый; belo vino, beli sneg, ovaj cvet je beo
   ``crvena``; |red|; красный; Crvena zvezda, crveno vino, crveni krst
   ``zelena``; |lightgreen|; Зеленый ; zelena salata, zeleni čaj
   ``svetlozelena``; |lightgreen|; светлозеленый;
   ``tamnozelena``; |darkgreen|; темнозеленый;
   ``plava``; |blue|; голубой/синий; plavo nebo, plava kosa
   ``svetloplava``; |lightblue|; голубой;
   ``tamnoplava``; |darkblue|; синий;
   ``žuta``; |yellow|; желтый; žuto sunce, žuti list
   ``braon``, ``smeđa``; |brawn|; коричневый; braon/smeđa kosa, braon/smeđi šećer
   ``tamnosmeđa``; |brawn-dark|; темно-коричневый;
   ``svetlosmeđa``; |brawn-light|; светло-коричневый;
   ``roze``, ``ružičasta``; |rose|; розовый; roze vino
   ``ljubičasta``; |violet|; фиолетовый; ljubičasta sveska
   ``narandžasta``; |orange|; оранжевый; ovaj sok je narandžast
   ``siva``; |grey|; серый; siva mačka
   ``bež``; |beige|; бежевый;
   ``tirkizna``; |turquoise|; бирюзовый;
   ``sivo-maslinasto``; |army-green|; серо-оливковый;


