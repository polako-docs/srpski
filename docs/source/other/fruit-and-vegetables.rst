.. _fruit-and-vegetables


Овощи и фрукты / Povrće i voće
=======================================

Овощи / Povrće
---------------------------------------

.. csv-table:: 
   :widths: 20, 20
   :delim: ;

   ``bundeva``, ``tikva``; тыква
   ``krompir``; картофель
   ``paradajz``; помидор
   ``tikvica``; кабачок
   ``krastavac``; огурец
   ``kupus``; капуста
   ``beli luk``; чеснок
   ``сrni luk``; белый репчатый лук
   ``crveni luk``; красный репчатый лук
   ``grašak``; горох
   



Фрукты / Voće
---------------------------------------


.. csv-table:: 
   :widths: 20, 20
   :delim: ;

   ``jabuka``; яблоко
   ``kruška``; груша
   ``banana``; банан
   ``limun``; лимон
   