.. _morati-trebati:

=======================================
Должен, Требуется / Morati, Trebati
=======================================


``Morati`` - быть должным, обязанным (что-то сделать). Ja moram - Я должен.


* Ja moram da radim.
* Ti moraš da radiš.
* On mora da radi.
* Mi moramo da radimo.
* Vi morate da radite.
* Oni moraju da rade.


``Trebati`` - следовать, надлежать, требоваться, нуждаться, иметь надобность.

В конструкции с глаголами „мне надо (что-то делать)“ используется безличная форма treba:

* Ja treba da radim -- Мне надо работать.
* Ti treba da radiš.
* On treba da radi.
* Mi treba da radimo.
* Vi treba da radite.
* Oni treba da rade.


**Отрицание**

В отрицательных конструкциях глагол morati означает только „быть не обязанным (что-то сделать)“ и не означает „быть не должным (что-то сделать).

Значение „быть не должным (что-то сделать)“ выражается глаголом trebati: „ja ne treba da radim“.

* Ja moram da radim -- Я должен работать. Я обязан работать.
* Ja treba da radim -- Мне надо/нужно работать.

* Ja ne moram da radim -- Я не обязан работать.
* Ja ne treba da radim -- Я не должен работать. Мне не надо/не нужно работать.