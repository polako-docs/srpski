.. _prevoz:

==========================================
Транспорт / Prevoz
==========================================


.. csv-table:: 
   :widths: 30, 60
   :delim: ;

   ``prevoz``; транспорт, транспортировка, перевозка
   ``kola``; машина, траспортное средство
   ``auto``; машина
   ``kamion``; грузовик, фура
   ``karavan``; фургон
   ``kombi``; микроавтобус
   ``prikolica``; прицеп
   ``teretno vozilo``; грузовик, грузовой автомобиль
   ``teretni saobraćaj``; грузовой транспорт, грузоперевозка
   ``teret``; груз
   ``kiper``; кузов, самосвал
   ``krilo``; крыло
   ``krov``; крыша
   ``kočnica``; тормоз
   ``auspuh``; выхлопная труба
   ``branik``; бампер
   ``dizel``; дизель
   ``benzin``; бензин
   ``brisači``; дворники
   ``far, svetlo``; фара, свет
   ``gas``; газ
   ``gepek, prtljažnik``; багажник
   ``gorivo``; топливо
   ``guma``; шина
   ``točak``; колесо
   ``hauba``; капот
   ``karoserija, šasija``; кузов
   ``lanac``; цепь
   ``motor``; двигатель, мотор
   ``papučica``; педаль
   ``volan``; руль
   ``menjač brzine``; переключатель скоростей
   ``poluga``; рычаг
   ``dizalica``; домкрат, кран
   ``pumpa``; насос
   ``presvlaka``; чехлы
   ``felni``; диски
   ``aluminijumske felne``; литые диски
   ``čelične felne``; шатмповки
   ``registarske tablice``; регистрационные номера
   ``žmigavac``; указатель поворота, поворотник
   ``ulje``; масло
   ``kočiona tečnost``; тормозная жидкость
   ``antifriz, rashladna tečnost``; антифриз, охлаждающая жидкость
   ``tečnost za pranje vetrobranskog stakla``; омывающая жидкость
   ``vetrobransko staklo``; ветровое/лобовое стекло
   ````;
   ````;

