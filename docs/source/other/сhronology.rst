.. _сhronology:


=======================================
Даты, время, дни недели и т.д.
=======================================



Время суток
---------------------------------------


.. csv-table::
   :widths: 20, 30
   :delim: ; 

   ``dan``; день, сутки 
   ``jutro``; утро
   ``doručak``; завтрак
   ``podne``; полдень
   ``ručak``; обед
   ``veče``; вечер 
   ``večera``; ужин
   ``noć``; ночь
   ``ponoć``; полночь


----

.. csv-table::
   :widths: 20, 30
   :delim: ;

   ``ujutru``; утром (в принципе)
   ``danju``; днем
   ``prepodne``; в первой половине дня, до обеда
   ``popodne``; во второй половине дня, после обеда
   ``uveče``; вечером
   ``noću``; ночью
   ``do kasno``; допоздна


----

.. csv-table::
   :widths: 20, 30
   :delim: ;

   ``sutra``; завтра с утра
   ``danas``; сегодня (днем)
   ``večeras``; сегодня вечером
   ``noćas``; сегодня ночью
   ``juče``; вчера
   ``sinoć``; вчера вечером, вчера ночью

----

.. csv-table:: 
   :widths: 20, 30
   :delim: ;

   ``nikad``; никогда
   ``retko``; редко
   ``ponekad``; иногда
   ``često``; часто
   ``uvek``; всегда
   ``skoro nekad``; почти никогда
   ``skoro uvek``; почти всегда
   ``uskoro``; вскоре



Моменты времени
---------------------------------------

.. csv-table:: 
   :widths: 20, 30
   :delim: ;

   ``doba``; эпоха, пора, время (в общем)
   ``godišnje doba``; время года
   ``trenutak``; момент, мгновение
   ``sekund``; секунда
   ``minut``; минута
   ``sati``; время
   ``sat``; час, часы (которые показывают время)
   ``pola sata``; полчаса
   ``dan``; сутки, день
   ``nedelja``, ``sedam dana``; неделя
   ``godina``; год
   ``kalendarska godina``; календарный год
   ``vek``, ``stoleće``; век
   ``sezona``; сезон
   ``početak``; начало
   ``kraj``; конец
   ``sredina``; середина


----

.. csv-table:: 
   :widths: 20, 30
   :delim: ;

   ``једном дневно``; раз в день
   ``двапут дневно``; два раза в день
   ``трипут дневно``; три раза в день
   ``једном недељно``; раз в неделю
   ``једном месечно``; раз в месяц
   ``једном годишње``; раз в год
   ``radni dani``; рабочие дни, будни
   ``neradni dani``; нерабочие дни
   ``svaki drugi dan``; через день, раз в два дня
   ``današnji dan``; сегодняшни день
   ``čitav dan``; весь день
   ``čitav nedelja``; вся неделя
   ``do dan danas``; до сих пор
   ``u toku dana``; в течение дня
   ``Koliko to košta po danu?``; Сколько это стоит в сутки?



Дни недели 
---------------------------------------


.. csv-table:: 
   :widths: 20, 20
   :delim: ;

   ``sedam dana``, ``nedelja``; неделя
   ``ponedeljak``; понедельник
   ``utorak``; вторник
   ``sreda``; среда
   ``četvrtak``; четверг
   ``petak``; пятница
   ``subota``; суббота
   ``nedelja``; воскресенье
   ``vikend``; выходной (именно суббота/воскресенье)
   ``slobodan dan``, ``neradni dan``; свободный день, нерабочий день


.. attention:: ``vikend`` ≠ ``slobodan dan``


.. csv-table:: 
   :widths: 20, 20
   :delim: ;

   ``ponedeljkom``; по понедельникам
   ``utorkom``; по вторникам
   ``sredom``; по средам
   ``četvrtkom``; по четвергам
   ``petkom``; по пятницам
   ``subotom``; по субботам
   ``nedeljom``; по воскресеньям



Времена года (Годишње добе)
---------------------------------------

.. csv-table:: 
   :widths: 20, 30
   :delim: ;

   ``пролеће``; весна
   ``лето``; лето
   ``jесен``; осень
   ``зима``; зима
