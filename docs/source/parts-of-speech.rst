.. parts-of-speech:

=====================================================
Части речи / Vrste reči
=====================================================

В сербском языке выделяют десять частей речи (врста речи):


.. csv-table::
   :widths: 40, 40
   :delim: ;
   :header: "Русский", "Srpski"

   Существительное; ``Imenica``
   Прилагательное; ``Pridev``
   Глагол; ``Glagol``
   Местоимение; ``Zamenica``
   Числительные; ``Brojevi``
   Наречие; ``Prilog`` (мн. ``Prilozi``)
   Предлоги; ``Predlog`` (мн. ``Predlozi``)
   Союзы; ``Veznici``




Существительные / Imenice
---------------------------------------


Род / Rod
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Существительные изменяются по родам (rod):

.. csv-table::
   :widths: 30, 30, 30
   :delim: ;
   :header: "Русский", "Srpski", "Primer"


   Мужской род; ``Muški rod``; grad, pas
   Женский род; ``Ženski rod``; žena
   Средний род; ``Srednjirod``; selo


Число / Broj
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Существительные изменяются по числам:

.. csv-table::
   :widths: 30, 30, 30
   :delim: ;
   :header: "Русский", "Srpski", "Primer"

   Единственное число; ``Jednina``; grad, stvar
   Множественное число; ``Množina``; gradovi, žene
   Только форма множественного числа; ``Samo množina``; makaze
   Существительные, имеющие разную форму в единственном и множественно мчисле; ``Imenice koje imaju u jednini jedan, a u množini drugi oblik``; čovek (jednina) – ljudi (množina)



Склонение / Deklinacija
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Существительные изменяются по падежам, то есть склоняются (имају деклинацију):

.. csv-table::
   :widths: 30, 30, 40
   :delim: ;
   :header: "Падеж", "Padež", "Odgovaraju  na pitanja"

   Именительный (основная форма существительного); ``Nominativ`` (osnovni oblik imenice); ``Ko?``, ``Šta?`` (Есть Кто? Что?)
   Родительный (говорит о принадлежности); ``Genitiv`` (kazuje pripadnost); ``(Od) Koga?``, ``Čega``, ``Čiji`` (Нет Кого? Чего? Чей?)
   Дательный (показывает направление); ``Dativ`` (kazuje pravac i namenu); ``Kome?``, ``Čemu?`` (Рад Кому? Чему?)
   Винительный (показывает объект, на который направлено действие); ``Akuzativ`` (kazuje objekat); ``Koga?``, ``Šta (vidiš)?``, ``Kuda (ideš)?`` (Вижу Кого? Что?)
   Звательный (в русском как бы есть и как бы нет); ``Vokativ`` (služi za dozivanje i obraćanje); ``Hej!`` (Эй!)
   Творительный; ``Instrumental`` (kazuje društvo i sredstvo); ``(Sa) kim?`` ``Čim(e)?`` (Доволен Кем? Чем?)
   Предложный (называет объект мысли или речи); ``Lokativ`` (označava mesto i nepravi objekat); ``Gde``, ``O kome``, ``O čemu`` (Думаю О ком? О чём? В ком? В чём? Где? (Местный))









Глаголы / Glagoli
---------------------------------------


Спряжение / Konjugacija
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Глаголы имеют спряжение (конјугацију), то есть изменяются по наклонениям, временам и лицам, а в прошедшем времени и сослагательном наклонении в единственном числе — по родам (вместо изменения по лицам); и по другим грамматическим категориям. 


Вид глагола / Glagolski vid
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Несовершенные глаголы / Nesvršene glagoli
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Глаголы несовершенного вида (nesvršene (imperfektivne)) обозначают процесс или повторяющееся действие. Проверочный вопрос в русском – что делать?

Совершенные / Svršene
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Глаголы совершенного вида (svršene (perfektivne)) обозначают законченное или одноразовое действие. Проверочный вопрос в русском – что сделать?