.. _verbs:


=======================================
Глаголы / Glagoli
=======================================


.. toctree::
   :maxdepth: 2

   verbs/biti
   verbs/group-i-e
   verbs/group-a-aju
   verbs/group-e-u
   verbs/reflexive_verbs
   verbs/verb-aspects
   verbs/verbs-of-movement
   verbs/imperativ


