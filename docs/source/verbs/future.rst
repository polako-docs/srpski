.. _future:

======================================================
Будущее время / Budućnost
======================================================

1. ja ću + infinitiv (raditi, čitati, napuniti...)
2. ti ćeš + inf
3. on, ona će + inf

množina
1. mi ćemo + inf
2. vi ćete + inf
3. oni će + inf

infinitivna osnova glaglova napuniti ->
napuniti = napuni - ti = napuni -

nastavci, koji su buduće vreme glagola hteti:

1. ću        1. ćemo
2. ćeš     2. ćete
3. će       3. će

infinitivno osnova + buduće vreme glagola hteti

1. napuniću
2. napunićeš
...