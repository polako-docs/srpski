.. _group-1:

========================================================
1. Глаголы с фонетическими изменения в корне
========================================================

.. csv-table:: 
   :widths: 20, 30
   :delim: ;
   :header: "Инфинитив", "1-е лицо"

   ``pisati``; ja ``pišem``
   ``lagati``; ja ``lažem``
   ``kretati``; ja ``krećem``
   ``stizati``; ja ``stižem``
   ``kazati``; ja ``kažem``
   ``plakati``; ja ``plačem``
   ``skakati``; ja ``skačem``



``pisati``
---------------------------------------

``pisati`` -- писать.

.. csv-table:: 
   :widths: 40, 40
   :delim: ;

   Ja ``pišem`` poruku.; Mi ništa ne ``pišemo``.
   Ti ``piše`` mejl.; Vi ne volite da ``pišete``.
   Pisac ``piše`` knjigu.; Oni nešto ``pišu``.


``stizati``
---------------------------------------

``stizati`` -- успевать, прибывать.

.. csv-table:: 
   :widths: 40, 40
   :delim: ;

   Ja uskoro ``stižem``; Mi ``stižemo`` za pet minuta.
   Kada ti ``stižeš``; Da li vi ``stižete``?
   Ona ništa ne ``stiže``; Oni sve ``stižu``?

Примеры:

.. csv-table:: 
   :widths: 40, 40
   :delim: ;

   ``Stižeš li na vreme?``; Успеваешь ли вовремя?
   ``Stižem!``; Успеваю!
   ``Kad stižete?``; Когда прибудете?
   ``Stižemo za pet minuta!``; Прибудем через пять минут.
   ``Ništa ne stižem!``; Ничего не успеваю!


``kazati``
---------------------------------------

``kazati`` -- сказать, говорить.

.. csv-table::
   :widths: 40, 40
   :delim: ;

   Mogu li ja da ``kažem``; Mi hoćemo nešto da ``kažemo``.
   Šta ti ``kažeš``; Šta vi ``kažete``?
   On ``kaže`` da sam ja u pravu; Oni ``kažu`` da to nije tačno?

Примеры:

.. csv-table:: 
   :widths: 40, 40
   :delim: ;

   ``Kazi``; Скажи
   ``Šta kažeš?``; Что говоришь?
   ``Ko to kaže?``; Кто это сказал?
   ``Kako se kaže na srpskom?``; Как это говорится/называется по-сербски?


.. note:: Когда передаешь чужие слова, то надо использовать конструкцию: ``Ti kažeš da imaš mačku``.


``lagati``
---------------------------------------

``lagati`` -- врать.

.. csv-table::
   :widths: 40, 40
   :delim: ;

   Ja nikad ne ``lažem``.; Mi nećemo da ``lažemo``.
   Zašto ti ``lažeš``; Vi ne treba da lažete.
   Taj političar samo ``laže``; Oni često ``lažu``.


Примеры:

.. csv-table:: 
   :widths: 40, 40
   :delim: ;

   ``To je laž! To nije istina.``; Это ложь, это не правда.
   ``Ko laže, taj i krade.``; Кто лжет, тот и крадет.



``plakati``
---------------------------------------

``plakati`` -- плакать.

.. csv-table::
   :widths: 40, 40
   :delim: ;

   Ja retko ``plačem``.; Mi nikad ne ``plačemo``.
   Zašto ti ``plačeš?``.; Zašto vi ``plačete?``.
   Ona ``plače`` za svaku sitnicu.; Oni sada ``plaču``.


Примеры:

* ``Nemoj da plačeš!`` -- не плачь.

``sitnica`` -- пустяк.



``skakati``
---------------------------------------

``skakati`` -- прыгать.

.. csv-table::
   :widths: 40, 40
   :delim: ;

   Ja ``skačem`` u vodu.; Mi ``skačemo`` visoko.
   Ti ``skačeš`` na treningu.; Vi ne ``možete`` da skačete?
   Konj može da ``skače``.; Deca ``skaču`` u parku.


Примеры:

* ``Nemoj da skačeš!`` -- не прыгай.



``kretati``
---------------------------------------

``kretati`` -- отправляться.


.. csv-table::
   :widths: 40, 40
   :delim: ;

   Ja sada ``krećem`` na posao.; Mi sutra ``krećemo`` na put.
   Kda ti ``krećeš`` na put?; Za koliko vi ``krećete``?
   On ``kreće`` za pet minuta; Oni uskoro ``kreću`` kući.

Примеры:

* ``kretati se`` -- двигаться.


