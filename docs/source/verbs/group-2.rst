.. _group-2:

========================================================
2. Глаголы с беглыми гласными
========================================================


.. csv-table:: 
   :widths: 20, 30
   :delim: ;
   :header: "Инфинитив", "1-е лицо"

   ``zvati``; ja ``zovem``
   ``prati``; ja ``perem``


``zvati``
---------------------------------------

``zvati`` -- звать, зваться, называться, приглашать, звонить.

.. csv-table:: 
   :widths: 40, 40
   :delim: ;

   Ja se ``zovem`` Vesna; Mi ``zovemo`` direktora.
   Koga ti ``zoveš`` na rođendan?; Vi treba da ``zovete`` taksi.
   On hoće da ``zove`` policiju.; Oni retko ``zovu`` roditelje.


``prati``
---------------------------------------

``prati`` -- мыть с большим количеством воды, чистить.

.. csv-table:: 
   :widths: 40, 40
   :delim: ;

   Ja stalno ``perem`` sudove.; Mi ``peremo`` auto.
   Zašto ti ne ``pereš`` ruke?; Vi ``perete`` kosu svaki dan.
   Mašina ``pere`` veš.; Oni ``peru`` zube ujutru i uveče.



