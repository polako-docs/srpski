.. _group-3:

========================================================
3. Глаголы типа ``jesti``
========================================================


.. csv-table:: 
   :widths: 20, 30
   :delim: ;
   :header: "Инфинитив", "1-е лицо"

   ``jesti``; ja ``jedem``
   ``ići``; ja ``idem``



``jesti``
--------------------------------------

``jesti`` -- есть, кушать.

.. csv-table::
   :widths: 10, 20, 10, 20
   :delim: ;

   ja; ``jedem``; mi; ``jedemo``
   ti; ``jedeš``; vi; ``jedete``
   on/ona/oni; ``jede``; oni/one/ona; ``jedu``


.. warning:: Не путать с глаголом "ехать". В сербском языке нет отдельного глагола, обозначающего езду. Во всех случаях используется глагол ``ići`` (``idem``).

``ići``
--------------------------------------

``ići`` -- идти, ехать, ездить, ходить.


.. csv-table::
   :widths: 10, 20, 10, 20
   :delim: ;

   Ja danas idem u pozorište.; Mi idemo na kafu.
   Ti treba da ideš na posao.; Kada vi idete u Moskvu?
   On ide u Ameriku.; Moja deca idu u školu.


.. csv-table::
   :widths: 30, 30
   :delim: ;

   ``Gde ideš?``; Куда идешь?
   ``Ne idem nigde.``; Никуда не иду.
   ``Idemo na kafu!``; Идем пить кофе.
   ``Idemo na more!``; Погнали на море.









``biti``
--------------------------------------

``biti`` -- быть.

В модальных конструкция (в конструкциях DA) глагол ``biti`` имеет формы:

.. csv-table::
   :widths: 10, 20, 10, 20
   :delim: ;

   ja; ``budem``; mi; ``budemo``
   ti; ``budeš``; vi; ``budete``
   on/ona/oni; ``bude``; oni/one/ona; ``budu`` 

При употреблении выражений: „я хочу быть..., могу быть..., должен быть...“ и т.п. существительное употребляется в именительном падеже.

.. csv-table::
   :widths: 10, 40
   :delim: ;

   Moram da;
   Treba da;
   Zelim da;\+ budem + именительный падеж.
   Hoću da;
   Mogu da;

Примеры:

* ``Mi ne želimo da budemo ovde``.
* ``Vi morate da budete vatrogasac``





``hteti`` 
--------------------------------------

``hteti`` -- хотеть. В первом лице ед. числа заканчивается на ``u``.

.. csv-table::
   :widths: 10, 20, 10, 20
   :delim: ;

   ja; ``hoću`` ; mi; ``hoćemo``
   ti; ``hoćeš`` ; vi; ``hoćete``
   on/ona/oni; ``hoće``; oni/one/ona; ``hoće``

.. warning:: Нельзя говорить "не хочу", есть отдельный глагол ``neću``.



``ne hteti``
--------------------------------------

``ne hteti`` -- не хотеть.


.. csv-table::
   :widths: 10, 20, 10, 20
   :delim: ;

   ja; ``neću`` ; mi; ``nećemo``
   ti; ``nećeš`` ; vi; ``nećete``
   on/ona/oni; ``neće``; oni/one/ona; ``neće``


В сербском, в отличии от русского языка нельзя задавать вопрос через отрицание.

* Недопустимо: ``Nećeš li kafu?``
* Правильно: ``Hoćeš li kafu?``


``moći``
--------------------------------------

``moći`` -- мочь, иметь возможность.


.. csv-table::
   :widths: 10, 20, 10, 20
   :delim: ;

   ja; ``mogu``; mi; ``možemo``
   ti; ``možeš``; vi; ``možete``
   on/ona/oni; ``može``; oni/one/ona; ``mogu`` 


.. warning:: Не надо путать ``može`` с ``možda``:

 * ``može`` = 1) можно; 2) он может; 3) выражение согласия
 * ``možda`` = возможно, может быть


