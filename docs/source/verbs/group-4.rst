.. _group-4:

========================================================
4. Глаголы типа ``piti``
========================================================


.. csv-table:: 
   :widths: 20, 30
   :delim: ;
   :header: "Инфинитив", "1-е лицо"

   ``piti``; ja ``pijem``
   ``čuti``; ja ``čujem``


``piti``
--------------------------------------

``piti`` -- пить.

.. csv-table::
   :widths: 10, 20, 10, 20
   :delim: ;

   ja; ``pijem`` ; mi; ``pijemo``
   ti; ``piješ`` ; vi; ``pijete``
   on/ona/oni; ``pije``; oni/one/ona; ``piju``


``čuti``
--------------------------------------

``čuti`` -- слышать, услышать.

.. csv-table::
   :widths: 40, 40
   :delim: ;

   Ja ``ništa`` ne čujem.; Mi vas ``čujemo``.
   Ti sve ``čuješ``.; Da li vi ``čujete``?
   On ne ``čuje`` dobro.; Oni ``čuju`` neke glasove.

Примеры:

.. csv-table::
   :widths: 40, 40
   :delim: ;

   ``Da li me čujete?``; Вы меня слышите.
   ``Ne jujem vas.``; Не слышу вас.
   ``Prvi put čujem.``; В первый раз слышу.






