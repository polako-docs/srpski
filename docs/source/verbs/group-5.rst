.. _group-5:

========================================================
5. Глаголы типа ``razumeti``
========================================================

.. csv-table:: 
   :widths: 20, 30, 30
   :delim: ;
   :header: "Инфинитив", "1-е лицо", "3-е лицо, мн.ч."

   ``razumeti``; ja ``razumem``; oni ``razumeju``
   ``umeti``; ja ``umem``; oni ``umeju``
   ``smeti``; ja ``smem``; oni ``smeju``



``razumeti``
--------------------------------------------------------

``razumeti`` -- понимать.


.. csv-table:: 
   :widths: 30, 30
   :delim: ;

   Ja ``ništa`` ne razumem.; Mi sve ``razumemo``.
   Ti sve ``razumeš``?; Da li me vi ``razumete``?
   On ne ``razume`` srpski.; Oni dobro ``razumeju`` ruski.


``umeti``
--------------------------------------------------------

``umeti`` -- уметь.

.. csv-table:: 
   :widths: 30, 30
   :delim: ;

   Ja ``umem`` da kuvam.; Mi ``umemo`` da pišemo ćirilicu.
   Ti ništa ne ``umeš``; Vi sve ``umete``!
   On ne ``ume`` da priča; Oni ne ``umeju`` da čitaju.



``smeti``
--------------------------------------------------------

``smeti`` -- не бояться, не иметь запрета на что-либо.


.. csv-table:: 
   :widths: 30, 30
   :delim: ;

   Ja ``smem``; Mi ``smemo``
   Ti ``smeš``; Vi ``smete``
   On ``sme``; oni ``smeju``


Примеры:

* Smem da jedem slatkiše.
* Ne smem da jedem slatkiše.
* Smem da šetam sama po šumi = Ne bojim se da šetam po šumi.
* Ne smem da šetam sama po šumi = Bojim se da šetam po šumi.









