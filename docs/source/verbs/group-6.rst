.. _group-6:

========================================================
6. Глаголы типа ``davati``
========================================================


.. csv-table:: 
   :widths: 20, 30
   :delim: ;
   :header: "Инфинитив", "1-е лицо"

   ``davati``; ja ``dajem``
   ``prodavati``; ja ``prodajem``


``davati``
---------------------------------------------------------

``davati`` -- давать.

.. csv-table:: 
   :widths: 20, 30
   :delim: ;

   Ja ``dajem`` poklon.; Mi ne ``dajemo`` naš broj telefona.
   Ti ne ``daješ`` novac.; Vi nikom ništa ne ``dajete``.
   Banka ``daje`` kredit.; Oni ``daju`` popust.


Примеры:

* Daj mi to!
* Dajte mi jedan hleb, molim vas.


``prodavati``
---------------------------------------------------------

``prodavati`` -- продавать.


.. csv-table:: 
   :widths: 20, 30
   :delim: ;

   Ja ``prodajem`` auto.; Mi ne ``prodajemo`` ništa.
   Ti ``prodaješ`` telefon.; Vi ``prodajete`` kompjutere.
   Ona ``prodaje`` sladoled.; Oni ``prodaju`` maglu.

Примеры:

* prodavati maglu -- продавать воздух (дословно: туман).


``predavati``
---------------------------------------------------------

``predavati`` -- преподавать.

.. csv-table:: 
   :widths: 20, 30
   :delim: ;

   Ja ``predajem`` u školi.; Mi ne ``predajemo`` engleski.
   Ti ``predaješ`` istoriju.; Šta vi ``predajete``?
   Ona ``predaje`` srpski jezik.; Oni ``predaju`` na fakultetu.


``izdavati``
---------------------------------------------------------

``izdavati`` -- сдавать.

.. csv-table:: 
   :widths: 20, 30
   :delim: ;

   Ja ``izdajem`` stan na Vračaru.; Mi ``izdajemo`` garsonjeru.
   Ti ``izdaješ`` apartman.; Da li vi ``izdajete`` stan?
   On ``izdaje`` sobu u Budvi.; Oni ne ``izdaju`` stan.

* apartman -- апартаменты;
* garsonjera - однокомнатная квартира, студия.



