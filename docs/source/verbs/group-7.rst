.. _group-7:

========================================================
7. Глаголы с ``-ova-``, ``-iva-`` в корне
========================================================

Если в корне глагола есть ``-ova-`` или ``-iva-``, то в спряжении оно меняется на ``-yj``. Это есть и в русском языке: рисовать -- рисую (а не "рисоваю").

.. csv-table:: 
   :widths: 30, 30
   :delim: ;
   :header: "Инфинитив", "1-е лицо"

   ``putovati``; ja ``putujem``
   ``kupovati``; ja ``kupujem``



``putovati``
---------------------------------------------------------

``putovati`` -- путешествовать.

.. csv-table:: 
   :widths: 30, 30
   :delim: ;

   Ja ``putujem``; Mi ``putujemo``
   Ti ``putuješ``; Vi ``putujete``
   On ``putuje``; Oni ``putuju``


``kupovati``
---------------------------------------------------------

``kupovati`` -- покупать.

.. csv-table:: 
   :widths: 30, 30
   :delim: ;

   Ja često ``kupujem`` u Lidlu.; Mi ne ``kupujemo`` ništa.
   Da li ti ``kupuješ`` auto?; Gde vi ``kupujete`` meso?
   On ``kupuje`` stan.; Oni ``kupuju`` na pijaci.


``iznajmljivati``
---------------------------------------------------------

``iznajmljivati`` -- снимать, брать в аренду.

.. csv-table:: 
   :widths: 30, 30
   :delim: ;

   Ja ``iznajmljujem`` stan.; Mi ``iznajmljujemo`` kuću.
   Ti često ``iznajmljuješ`` auto.; Šta vi ``iznajmljujete``?
   Ona ne ``iznajmljuje`` stan.; Oni ``iznajmljuju`` vilu.



``doručkovati``
---------------------------------------------------------

``doručkovati`` -- завтракать.

.. csv-table:: 
   :widths: 30, 30
   :delim: ;

   Ja često ``doručkujem`` kašu.; Mi ``doručkujemo`` u devet.
   Šta ti ``doručkuješ``?; Kada vi ``doručkujete``?
   On nikad ne ``doručkuje``.; Oni ``doručkuju`` jaja.



``radovati se``
---------------------------------------------------------

``radovati se`` -- радоваться.

.. csv-table:: 
   :widths: 30, 30
   :delim: ;

   Ja se često ``radujem``.; Mi se ``radujemo`` svaki dan.
   Zašto se ti ``raduješ``?; Da li se vi ``radujete``?
   On se nikad ne ``raduje``.; Oni se ``raduju`` uspehu.




