.. _verb-aspects:

=========================================================
Вид глагола / Glagolski vid
=========================================================

Совершенные и несовершенные глаголы / Svršene i nesvršene glagoli
---------------------------------------------------------

Глаголы подращделяются на соверешенные и несовершенные. 

**Глаголы несовершенного вида** (nesvršene (imperfektivne)) обозначают процесс или повторяющееся действие. Проверочный вопрос в русском -- ``что делать?``

**Глаголы совершенного вида** (svršene (perfektivne)) обозначают законченное или одноразовое действие. Проверочный вопрос в русском -- ``что сделать?``

.. attention:: Глаголы совершенного вида не могут образовать настоящее время. Нельзя сказать ``Ja pročitam``, в такой форме глаголы совершенного вида можно употребляться только в конструкциях с ``да``, ``ако`` или ``када`` (например, ``Ja hoću da pročitam``, ``Ti moraš da pročitaš``).

1. Как правило, глагол совершенного вида обраузуется с помощью приставки:

.. csv-table:: 
   :widths: 20, 20, 20, 20
   :delim: ;
   :header: "nesvršene", "несовершенный", "svršene", "совершенный"

   ``raditi``; делать; ``uraditi``; сделать
   ``pisati``; писать; ``napisati``; написать
   ``čitati``; читать; ``pročitati``; прочитать
   ``pričati``; рассказывать; ``ispričati``; рассказать
   ``prati``; мыть; ``oprati``; вымыть
   ``kasniti``; опаздывать, задерживаться; ``zakasniti``; опоздать, задержаться
   ``kuvati``; варить, готовить; ``skuvati``; сварить, приготовить
   ``praviti``; делать; ``napraviti``; сделать
   ``učiti``; учиться; ``naučiti``; научиться
   ``gledati``; смотреть; ``pogledati``; посмотреть
   ``zvati``; звать, звонить; ``pozvati``; позвать, позвонить
   ``želeti``; хотеть, желать; ``poželeti``; пожелать
   ``žuriti``; спешить; ``požuriti``; поспешиить
   ``jesti``; есть; ``pojesti``; поесть, съесть, скушать
   ``piti``; пить; ``popiti``; выпить


2. Если глагол имеет суффиксы ``ova``, ``iva``, ``ava`` (kupovati):

.. csv-table:: 
   :widths: 20, 20, 20, 20
   :delim: ;
   :header: "nesvršene", "несовершенный", "svršene", "совершенный"

   ``kupovati``; покупать; ``kupiti``; купить
   ``iznajmljivati``; снимать; ``iznajmiti``; снять
   ``davati``; давать; ``dati``; дать
   ``prodavati``; продавать; ``prodati``; продать
   ``izdavati``; выдавать; ``izdati``; выдать
   ``zakazivati``; назначать; ``zakazati``; назначить
   ``naručivati``; заказывать; ``naručiti``; заказать

3. Глагол несовершенного вида имеет больше звуков А, чем глагол совершенного вида:

.. csv-table:: 
   :widths: 20, 20, 20, 20
   :delim: ;
   :header: "nesvršene", "несовершенный", "svršene", "совершенный"

   ``uzimati``; брать; ``uzeti``; взять
   ``plaćati``; платить; ``platiti``; заплатить
   ``sećati se``; вспоминать; ``setite se``; вспомнить
   ``skakati``; прыгать, скакать; ``skočiti``; прыгнуть, отскочить
   ``kretati``; двигаться; ``krenuti``; отправиться, выдвинуться
   ``stizati``; успевать, пребывать; ``stići``; успеть, прибыть
   ``spremati``; готовить; ``spremiti``; приготовить
   ``odmarati se``; отдыхать; ``odmoriti se``; отдохнуть
   ``sviđati se``; нравиться; ``svidati se``; понравиться


4. Глаголы с отличающимися основами (суплетивные):

.. csv-table:: 
   :widths: 20, 20, 20, 20
   :delim: ;
   :header: "nesvršene", "несовершенный", "svršene", "совершенный"

   ``dolaziti``; приходить, приезжать, доходить, доезжать;``doći``; прийти, приехать, дехать, дойти
   ``polaziti``; отправляться, начинать движение;``poći``; пойти, отправиться
   ``ulaziti``; входить, въезжать, заходить, заезжать;``ući``; войти, въехать, зайти, заехать
   ``izlaziti``; выходить, выезжать;``izaći``; выйти, выехать
   ``odlaziti``; уходить, уезжать;``otići``; уйти, уехать


Таблица глаголов
-----------------------------------

.. csv-table:: 
   :widths: 20, 20, 20
   :delim: ;
   :header: "Infinitiv", "Ja", "Prevod"

   ``raditi``; ``radim``; работать, делать
   ``uraditi``; ``uradim``; сделать
   ``praviti``; ``pravim``; создавать, делать, строить
   ``napraviti``; ``napravim``; создать, сделать, построить
   ``učiti``; ``učim``; учиться, учить, изучать
   ``naučiti``; ``naučim``; научиться, выучить, изучить
   ``pisati``; ``pišem``; писать
   ``napisati``; ``napišem``; написать
   ``pričati``; ``pričam``; говорить, рассказывать
   ``ispričati``; ``ispričam``; рассказать
   ``prate``; ``perem``; мыть
   ``oprati``; ``operem``; помыть, вымыть
   ``kasniti``; ``kasnim``; опаздывать
   ``zakasniti``; ``zakasnim``; опаздать
   ``kuvati``; ``kuvam``; варить, готовить
   ``skuvati``; ``skuvam``; сварить, приготовить
   ``gledati``; ``gledam``; смотреть
   ``pogledati``; ``pogledam``; посмотреть
   ``zvati``; ``zovem``; звать, звонить, приглашать
   ``pozvati``; ``pozovem``; позвать, позвонить, пригласить
   ``želeti``; ``želim``; хотеть, желать
   ``poželeti``; ``poželim``; захотеть, пожелать
   ``žuriti``; ``žurim``; спешить, торопиться
   ``požuriti``; ``požurim``; поспешить, поторопиться
   ``jesti``; ``jedem``; есть
   ``pojesti``; ``pojedem``; съесть
   ``piti``; ``pijem``; пить
   ``popiti``; ``popijem``; выпить
   ``kupovati``; ``kupujem``; покупать
   ``kupiti``; ``kupim``; купить
   ``iznajmljivati``; ``iznajmljivaen``; брать в аренду, снимать
   ``iznajmiti``; ``iznajmim``; взять в аренду, снять
   ``zakazivati``; ``zakazujem``; записываться, назначать
   ``zakazati``; ``zakažem``; записаться, назначить
   ``naručivati``; ``naručujem``; заказывать
   ``naručiti``; ``naručim``; заказать
   ``kasniti``; ``kasnim``; опаздывать
   ``zakasniti``; ``zakasnim``; опоздать
   ````; ````;
   ````; ````;