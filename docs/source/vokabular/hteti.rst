.. _hteti:

=============================================
hteti, ne hteti / хочеть, не хотеть
=============================================

Группа глаголов: ``-E/-U``

Подгруппа: ``3. Глаголы типа jesti``

Infinitive
-------------------------------------


``hteti`` – хотеть. 

Отрицательная форма образуется с помощью другого глагола.

``ne hteti`` – не хотеть. 


Настоящее/Прошедшее/Будущее
-------------------------------------

.. csv-table:: ``hteti`` 
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Present", "Perfect", "Future"

    Ja; hoću; hteo sam; hteću
    Ti; hoćeš; hteo/htela/htelo si; htećeš
    On/Ona/Ono; hoće; hteo/htela/htelo je; hteće
    Mi; hoćemo; hteli smo; htećemo
    Vi; hoćete; hteli ste; htećete
    Oni; hoću; hteli su; hteću


.. csv-table:: ``ne hteti`` 
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Present", "Perfect", "Future"

    Ja; neću; nisam hteo; neću
    Ti; nećeš; nisi hteo/htela/htelo; nećeš
    On/Ona/Ono; neće; nije hteo/htela/htelo; neće
    Mi; nećemo; nismo hteli; nećemo
    Vi; nećete; niste hteli; nećete
    Oni; neću; nisu hteli; neću



