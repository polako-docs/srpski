.. _nedostajati:

=========================================================
nedostajati / отсутствовать, нехватать
=========================================================

Неопределенная форма / Infinitiv 
----------------------------------------------------------

``nedostajati`` -- недоставать, отсутсвовать, не хватать.

Настоящее время / Prezent (sadašnje vreme)
----------------------------------------------------------

.. csv-table::
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Prezent", "Primer", "Prevod"

    ja; nedostajem; Nedostajem joj.; Nedostajem joj.
    ti; nedostaješ; Nedostaješ mi.; Мне не хватает тебя.
    on / ona / ono; nedostaje; Nedostaje mi dom.; Я скучаю по дому.
    mi; nedostajemo; Nedostajemo ti.; Ты скучаешь по нам.
    vi; nedostajete; Nedostajete mi.; Я скучаю по вам.
    oni / one / ona; nedostaju; Nedostaju mi moji roditelji.; Я скучаю по своим родителям.



Прошедшее время / Perfekat (prošlo vreme)
----------------------------------------------------------

.. csv-table::
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Prezent", "Primer", "Prevod"

    ja; nedostajao sam; ;
    ti; nedostajao si; ;
    on / ona / ono; nedostajao je; ;
    mi; nedostajali smo; ;
    vi; nedostajali ste; ;
    oni / one / ona; nedostajali su; ;



     	 		


Будущее время / Futur I (buduće vreme) 
----------------------------------------------------------