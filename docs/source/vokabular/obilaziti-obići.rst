.. _obilaziti-obići:

obilaziti, obići
===============================================================



Неопределенная форма / Infinitiv 
----------------------------------------------------------

* ``obilaziti`` --  обходить, объезжать, навещать (несовершенный вид)
* ``obići`` -- обойти, объехать, навестить (совершенный вид)


Настоящее время / Prezent (sadašnje vreme)
----------------------------------------------------------

* ``obilaziti`` --  обходить, объезжать, навещать (несовершенный вид)

.. csv-table::
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Prezent", "Primer", "Prevod"

    ja; obilazim; ;
    ti; obilaziš; ;
    on / ona / ono; obilazi; ;
    mi; obilazimo; ;
    vi; obilazite; ; 
    oni / one / ona; obilaze; ; 

* ``obići`` -- обойти, объехать, навестить (совершенный вид)

.. csv-table::
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Prezent", "Primer", "Prevod"

    ja; obiđem; ;
    ti; obiđeš; ;
    on / ona / ono; obiđe; ;
    mi; obiđemo; ;
    vi; obiđete; ; 
    oni / one / ona; obiđu; ; 



Прошедшее время / Perfekat (prošlo vreme)
----------------------------------------------------------

.. csv-table::
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Prezent", "Primer", "Prevod"

    ja; obilazio; ; 
    ti; obilazio; ; 
    on / ona / ono; obilazili; ;
    mi; obilazili; ;
    vi; obilazili; ; 
    oni / one / ona; obilazili; ;




Будущее время / Futur I (buduće vreme) 
----------------------------------------------------------