.. _odlaziti-otići:


odlaziti, otići -- уходить, уезжать, уехать, уйти
======================================================

Неопределенная форма / Infinitiv 
----------------------------------------------------------

* ``odlaziti`` -- уходить, уезжать (несовершенная форма)
* ``otići`` -- уехать, уйти (совершенная форма)


Настоящее время / Prezent (sadašnje vreme)
----------------------------------------------------------

``odlaziti`` -- уходить, уезжать (несовершенная форма)

.. csv-table::
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Prezent", "Primer", "Prevod"

    ja; odlazim; ; 
    ti; odlaziš; ; 
    on / ona / ono; odlazi; ;
    mi; odlazimo; ;
    vi; odlazite; ; 
    oni / one / ona; odlaze; ; 

``otići`` -- уехать, уйти (совершенная форма)

.. csv-table::
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Prezent", "Primer", "Prevod"

    ja; odem; ; 
    ti; odeš; Moraš do odeš na poslu. ; 
    on / ona / ono; ode; ;
    mi; odemo; ;
    vi; odete; ; 
    oni / one / ona; odu; ; 



Прошедшее время / Perfekat (prošlo vreme)
----------------------------------------------------------

.. csv-table::
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Prezent", "Primer", "Prevod"

    ja; odlazio; ; 
    ti; odlazio; ; 
    on / ona / ono; odlazili; ;
    mi; odlazili; ;
    vi; odlazili; ; 
    oni / one / ona; odlazili; ;


Будущее время / Futur I (buduće vreme) 
----------------------------------------------------------



Отглагольные существительные / Glagolske imenice
----------------------------------------------------------

``odlazak``:

* уход, отъезд, отбытие

.. note:: В русском языке слово уход имеет еще значение "уход за больным", в сербском для этого используется слово ``nega``. 

* дорого "туда" (``u odlasku``, ``u povratku`` -- обратно)

Повелительное наклонение
-------------------------------------