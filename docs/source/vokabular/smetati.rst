.. _smetati:

=====================================
smetati / мешать, беспокоить
=====================================

Группа глаголов: ``-A/-AJU``

Infinitive
-------------------------------------

``smetati`` -- мешать, беспокоить.


Настоящее/Прошедшее/Будущее
-------------------------------------

.. csv-table:: 
    :delim: ;
    :widths: 10, 10, 10, 10
    :header: "Lice", "Present", "Perfect", "Future"

    Ja; smetam; smetao sam; smetaću
    Ti; smetaš; smetao/smetala/smetalo si; smetaćeš
    On/Ona/Ono; smeta; smetao/smetala/smetalo je; smetaće
    Mi; smetamo; hteli smo; smetaćemo
    Vi; smetate; hteli ste; smetaćete
    Oni; smetaju; hteli su; smetaće


Повелительное наклонение
-------------------------------------

.. csv-table:: 
    :delim: ;
    :widths: 10, 10
    :header: "Lice", "Imperativ"

    Ti; smetaj
    Mi; smetajmo
    VI; smetajte



